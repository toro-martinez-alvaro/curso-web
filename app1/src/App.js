import React from "react";
import logo from "./logo.svg";
import "./App.css";
import react, { Component } from "react";
//estoy importando la herramienta "todos"(por hacer, to-do) desde el archivo "todos.json"
import { todos } from "./todos.json";
console.log(todos);
//arriba use el "console.log(todos)" para que en la consola me aparescan los "todos" que en la linea de arriba ya se habian importado del archivo "todos.json"

class App extends Component {
  constructor() {
    super();
    this.state = {
      todos
    }

  }

  render() {
    this.state.todos.map((todos));
    return (
      <div className="App">
      <nav className= "navbar navbar-dark bg-dark">
      <a href="" className= "text-white">
      
      </a>
      </nav>
        <img src={logo} className="App-logo" alt="logo" />
      </div>
    );
  }
}

export default App;
//EXPLICACION:el "constructor" es el metodo que primero se ejecuta incluso antes que "render"(antes que se pinte la pantalla). siempre que se crea un constructor se debe ejecutar un "super();" para poder heredar toda funcionalidad de react