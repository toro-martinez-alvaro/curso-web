//es la libreria de react, permite crear las interfaces web
//reactDOM es react enfocado al navegador
import React from 'react';
import ReactDOM from 'react-dom';
//siempre que se desee hacer una pagina web se debe importar la libreria de react y reactDOM como se ve arriba

//afecta todo el cuerpo del programa, desdde "index.css" se modificara visualmente el programa
import './index.css';

// importa una aplicacion desde "app.js" se encarga solo de arrancar la aplicacion
import App from './App';
import * as serviceWorker from './serviceWorker';
//"serviceWorker" hace que la aplicacion funcione aun sin coneccion a internet

ReactDOM.render(<App />, document.getElementById('root'));
//"ReactDOM" es la biblioteca de react enfocada al navegador. su metodo "render" permite es ayudar a React a pintar la interfaz en pantalla (renderizar la interfaz en pantalla. renderiza la app que hemos escrito, la "app.jsx" que importamos anteriormente )

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
